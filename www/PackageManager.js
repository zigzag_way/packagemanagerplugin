var PackageManager = {
    getInstalledApplications: function(success, fail) {
        return cordova.exec(success, fail, 'PackageManager', 'getInstalledApplications', []);
    },
    getRunningTasks: function(success, fail) {
        return cordova.exec(success, fail, 'PackageManager', 'getRunningTasks', []);
    },
    isTaskRunning: function(package, success, fail) {
        return cordova.exec(success, fail, 'PackageManager', 'isTaskRunning', [package]);
    },
    setUserID: function(user_id, device_id, success, fail) {
        return cordova.exec(success, fail, 'PackageManager', 'setUserID', [user_id, device_id]);
    }
}
module.exports = PackageManager;
