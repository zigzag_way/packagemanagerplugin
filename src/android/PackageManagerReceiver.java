package com.phonegap.plugins;

import android.content.*;
import android.net.http.AndroidHttpClient;
import android.preference.PreferenceManager;
import android.util.Log;
import org.apache.http.client.methods.HttpGet;

public class PackageManagerReceiver extends BroadcastReceiver {
    
    @Override
    public void onReceive(Context ctx, Intent intent) {
    	Log.d("PackageManagerReceiver", "onReceive");
        try {
        	String packageID = intent.getData().toString();
	    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
			String deviceID = settings.getString("device_id", "");
	        String userID = settings.getString("user_id", "");
			if (!packageID.startsWith("package:") && userID != "" && deviceID != "") return;
			packageID = packageID.substring(8);
	    	Log.d("PackageManagerReceiver", "packageID = " + packageID + ", ctx.getPackageName() = " + ctx.getPackageName() + ", not equal? " + !packageID.equals(ctx.getPackageName()));
			if (!packageID.equals(ctx.getPackageName()) && (intent.getAction() != ""))
				sendRequest(intent.getAction(), packageID, deviceID, userID);
		} catch (final Exception e) {
	    	Log.d("PackageManagerReceiver", "Exception");
            System.err.println("Exception: " + e.getMessage());
        }
    }

    private boolean sendRequest(String intent, String data, String deviceID, String userID) {
        final String url = "http://show.appcoins.mobi/" + "package_intent.php?uid=" + userID + "&device_id=" + deviceID + "&action=" + intent + "&package=" + data;
    	Log.d("PackageManagerReceiver", "sendRequest. url = " + url);
    	final AndroidHttpClient httpclient = AndroidHttpClient.newInstance("Android");
	    new Thread(new Runnable() {
	        public void run() {
		    	try {
				        	httpclient.execute(new HttpGet(url));
				} catch (Exception e) {
					Log.e("PackageManagerReceiver", "Network exception", e);
				} finally {
					httpclient.close();
				}
	        }
	    }).start();
		
		return true;
    }
}
