package com.phonegap.plugins;

import java.util.ArrayList;
import java.util.List;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;

public class PackageManagerPlugin extends CordovaPlugin {
	private PackageManager packageManager = null;

	class PInfo {
		 private String appname = "";
		 private String pname = "";
		 private String versionName = "";
		 private int versionCode = 0;
	}
    
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callback) throws JSONException{
    	Log.v("PackageManagerPlugin", "PackageManagerPlugin execute method called");
        packageManager = cordova.getActivity().getPackageManager();
        //applist = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
        
        if (action.equals("getInstalledApplications")) {
            try {
                ArrayList<PInfo> res = new ArrayList<PInfo>();        
                String resStr = "";
                List<PackageInfo> packs = packageManager.getInstalledPackages(0);
                for (int i = 0; i < packs.size(); i++) {
                    PackageInfo p = packs.get(i);
                    if (isSystemPackage(p))
                        continue;
                    PInfo newInfo = new PInfo();
                    newInfo.appname = p.applicationInfo.loadLabel(packageManager).toString();
                    newInfo.pname = p.packageName;
                    newInfo.versionName = p.versionName;
                    newInfo.versionCode = p.versionCode;
                    res.add(newInfo);
                    if (i > 1)
                        resStr += ", " + newInfo.pname;
                    else
                    	resStr = newInfo.pname;
                }
                Log.d("PackageManagerPlugin", "Number of installed apps (w/o system) = " + res.size());
                callback.success(resStr); //returs string of app package name delimited with comma
                return true;
            } catch (final Exception e) {
                System.err.println("Exception: " + e.getMessage());
                callback.error(e.getMessage());
            }
        } else if (action.equals("getRunningTasks")) {
            try {
                callback.success(getRunningAppProcesses()); //returs string of app package name delimited with comma
                return true;
            } catch (final Exception e) {
                System.err.println("Exception: " + e.getMessage());
                callback.error(e.getMessage());
            }
        } else if (action.equals("isTaskRunning")) {
            try {
                if (getRunningAppProcesses().indexOf(args.getString(0)) != -1)
                    callback.success("true");
                else
                	callback.success("false");
                return true;
            } catch (final Exception e) {
                System.err.println("Exception: " + e.getMessage());
                callback.error(e.getMessage());
            }
        } else if (action.equals("setUserID")) {
            Log.d("PackageManagerPlugin", "setUserID = " + args.getString(0) + ", " + args.getString(1));
        	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.cordova.getActivity());
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("user_id", args.getString(0));
            editor.putString("device_id", args.getString(1));
            editor.commit();
            return true;
        }

        return false;
    }

	  /**
	  * Return whether the given PackgeInfo represents a system package or not.
	  * User-installed packages (Market or otherwise) should not be denoted as
	  * system packages.
	  *
	  * @param pkgInfo
	  * @return boolean
	  */
    private boolean isSystemPackage(PackageInfo pkgInfo) {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true : false;
    }

    private String getRunningAppProcesses() {
        List<RunningAppProcessInfo> apps = ((ActivityManager) cordova.getActivity().getSystemService(cordova.getActivity().ACTIVITY_SERVICE)).getRunningAppProcesses();
        String packages = "";
        if (apps != null && apps.size() > 0) {
            for (int i = 0; i < apps.size(); i++)
                if (apps.get(i).importance == android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND ||
                    apps.get(i).importance == android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND ||
                    apps.get(i).importance == android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_PERCEPTIBLE ||
                    apps.get(i).importance == android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE) {
                    //Log.v("RunningAppProcessInfo", apps.get(i).processName + ", " + apps.get(i).importance);
                    if (i == 0) packages = apps.get(i).processName;
                    else packages += ", " + apps.get(i).processName;
                }
        } else {
            // In case there are no processes running (not a chance :))
            Log.v("RunningAppProcessInfo", "No application is running");
        }
        return packages;
    }
}
